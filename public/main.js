var subject = 'Suspendez l\'expulsion des familles sur la commune de Rosny-sous-Bois à proximité de l\'A86'
var recipient = '\
exemple@exemple.ex <exemple@exemple.ex>,\
exemple@exemple.ex <exemple@exemple.ex>,\
'
var recipient_cc = '\
exemple@exemple.ex <exemple@exemple.ex>,\
exemple@exemple.ex <exemple@exemple.ex>,\
exemple@exemple.ex <exemple@exemple.ex>,\
'

function onLoad () {
  document.getElementById('arguments').addEventListener('click', update)
  document.getElementById('name').addEventListener('change', update)
  document.getElementById('city').addEventListener('change', update)
  document.getElementById('send').addEventListener('click', showSendTip)
  document.getElementById('showEmail').addEventListener('click', showEmail)
  update()
}

function update () {
  var body = replaceTripleDots(buildBody())
  setMailtoHref(body)
  document.getElementById('subjectText').innerHTML = subject
  document.getElementById('recipientText').innerHTML = recipient
  document.getElementById('bodyText').innerHTML = body.replace(/\n/g, '<br>')
}

function elText (elId) {
  return document.getElementById(elId).textContent
}

function getSelectedArgumentsText () {
  var elements = document.querySelectorAll('#arguments li')
  return [].filter.call(elements, function (el) {
    return el.childNodes[0].checked
  })
  .map(function (el) {
    return el.textContent
  })
  .join('\n\n')
}

function getField (id) {
  var el = document.getElementById(id)
  return el.value || el.attributes.placeholder.value
}

function buildBody () {
  return elText('intro-1') + '\n\n' + elText('intro-2') + '\n\n' + getSelectedArgumentsText() + '\n\n' + elText('conclusion-1') + '\n\n' + elText('conclusion-2') + '\n\n' + getField('name')+ '\n' + getField('city') + '\n'
}

function replaceTripleDots (text) {
  return text
  // Replace the first instance
  .replace('… ', '')
  // and all the others
  .replace(/… /g, '')
}

function buildMailtoHref (body) {
  return 'mailto:' + recipient + '?subject=' + encodeURIComponent(subject) + '&body=' + encodeURIComponent(body) + '&cc=' + recipient_cc
}

function setMailtoHref (body) {
  var href = buildMailtoHref(body)
  document.getElementById('send').attributes.href.value = href
}

function hide (id) {
  var el = document.getElementById(id)
  el.className += ' hidden'
}

function show (id, className) {
  var el = document.getElementById(id)
  el.className = el.className.replace('hidden', '').trim()
}

function showSendTip () {
  show('preparingEmailFailedTip')
  hide('emailCanBeModifiedTip')
}

function showEmail (event) {
  document.getElementById('email').className = ''
  event.preventDefault()
}

document.addEventListener('DOMContentLoaded', onLoad, false)

console.log('Salut ! Pour tout rapport de bug ou suggestion : https://framagit.org/alternatiba-rosny/generateur-mail-roms/issues')
